from flask import Flask, request
from flask_restplus import Resource, Api
from jira_sro_etl import factories
from sro_db.service.core.service import ApplicationService, ConfigurationService
from sro_db.service.organization.service import OrganizationService


app = Flask(__name__)
api = Api(app)

todos = {}
# a5669938-64f7-4528-b1d8-98e144d5b513
# Considering that the DB has already been created and filled
applicationService = ApplicationService()
application = applicationService.retrive_by_name('jira')

def getData(organization_id):
    # Hardcoded the organization uuid for now
    # Data is used for acess the JiraX API
    # And acess the BD 
    organizationService = OrganizationService()
    organization = organizationService.get_by_uuid(organization_id)

    configurationService = ConfigurationService()
    configuration = configurationService.retrive_by_organization_and_application(organization, application)[0]

    data = {
        'user': configuration.user,
        'key': configuration.secret,
        'url': configuration.url,
        'organization_id': organization.uuid,
        'configuration_id': configuration.uuid,
    }
    return data


# Classes that reference the ontology and BD
class User(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        user = factories.userFactory()
        user.do(data)

class ScrumProject(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        scrum_project = factories.scrum_projectFactory()
        scrum_project.do(data)     

class Scrum_project_team(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        scrum_project_team = factories.scrum_project_teamFactory()
        scrum_project_team.do(data)

class Scrum_development_team(Resource):
     def post(self, organization_id):
        data = getData(organization_id)
        scrum_development_team = factories.scrum_development_teamFactory()
        scrum_development_team.do(data)        

class Sprint(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        sprint = factories.sprintFactory()
        sprint.do(data)

class Scrum_development_task(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        scrum_development_task = factories.scrum_development_taskFactory()
        scrum_development_task.do(data)
        
class Epic(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        epic = factories.epicFactory()
        epic.do(data)

class Product_backlog(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        product_backlog = factories.product_backlogFactory()
        product_backlog.do(data)

class Sprint_backlog(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        sprint_backlog = factories.sprint_backlogFactory()
        sprint_backlog.do(data)

class Team_member(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        team_member = factories.team_memberFactory()
        team_member.do(data)
        

class User_story(Resource):
    def post(self, organization_id):
        data = getData(organization_id)
        user_story = factories.user_storyFactory()
        user_story.do(data)
        

# Routes that call the class methods
api.add_resource(User, '/user/<string:organization_id>')
api.add_resource(ScrumProject, '/scrumproject/<string:organization_id>')
api.add_resource(Scrum_project_team, '/scrumprojectteam/<string:organization_id>')
api.add_resource(Scrum_development_team, '/scrumdevelopmentteam/<string:organization_id>')
api.add_resource(Team_member, '/teammember/<string:organization_id>')
api.add_resource(Sprint, '/sprint/<string:organization_id>')
api.add_resource(Scrum_development_task, '/scrumdevelopmenttask/<string:organization_id>')
api.add_resource(Epic, '/epic/<string:organization_id>')
api.add_resource(Product_backlog, '/productbacklog/<string:organization_id>')
api.add_resource(Sprint_backlog, '/sprintbacklog/<string:organization_id>')
api.add_resource(User_story, '/userstory/<string:organization_id>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

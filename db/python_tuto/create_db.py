from sro_db.config.config import Config
from sro_db.model.activity.models import *
from sro_db.service.activity.service import *
from sro_db.model.core.models import *
from sro_db.service.core.service import *
from sro_db.model.stakeholder_participation.models import *

conf = Config()
conf.create_database()

risks = ["high", "medium", "normal"]

activity_types = ["deployment", "analysis", "design", "development", "documentation", "requirements", "testing"]
developmentTaskTypeService = DevelopmentTaskTypeService()


for data in activity_types:

    task_type = DevelopmentTaskType()
    
    task_type.name = data
    task_type.description = data
    developmentTaskTypeService.create(task_type)

priorityService = PriorityService()
riskService = RiskService()

for data in risks:

    priority = Priority()
    
    priority.name = data
    priority.description = data
    priorityService.create(priority)
    
    risk = Risk()
    risk.name = data
    risk.description = data

    riskService.create(risk)


applicationTypeService = ApplicationTypeService()

project_management = ApplicationType()
project_management.name = "Project Management"
project_management.description = "Project Management"
applicationTypeService.create(project_management)

time_tracking_management = ApplicationType()
time_tracking_management.name = "Time Tracking Management"
time_tracking_management.description = "Time Tracking Management"
applicationTypeService.create(time_tracking_management)

application_service = ApplicationService()

#Criando o TFS
tfs = Application()
tfs.name = "tfs"
tfs.description = "tfs"
tfs.application_type = project_management
application_service.create(tfs)

#Criando o Jira
jira = Application()
jira.name = "jira"
jira.description = "jira"
jira.application_type = project_management
application_service.create(jira)

#Criando o clockify
clockify = Application()
clockify.name = "clockify"
clockify.description = "clockify"
clockify.application_type = time_tracking_management
application_service.create(clockify)

    
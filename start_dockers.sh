#!/bin/bash
set -e
echo Setando variáveis de ambiente
export USERDB=postgres
export PASSWORDDB=1234
export HOST=localhost
export DBNAME=sro_ontology

echo Criando network myNetwork
docker network create myNetwork

echo Iniciando container postgres, webservice, app
docker-compose -f db/docker-compose.yml up -d

echo Criando organização
echo Entrando na env
source db/env/bin/activate
echo Executando create_db
python3 db/python_tuto/create_db.py
# python3 db/python_tuto/create_org.py
docker network connect myNetwork db_postgres_1 

echo Iniciando container Rabbit
docker run --network=myNetwork -d -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management

echo Iniciando container Realtime_webservice
docker build realtime_webservice/ -t realtime_webservice
docker run --network=myNetwork -d --name=realtime_webservice realtime_webservice

echo Iniciando container Metabase
docker run --network=myNetwork -d -p 3000:3000 --name metabase metabase/metabase

echo Iniciando container Jira_hub
docker build jira_hub/ -t jira_hub
docker run --net=host -d --name=jira_hub jira_hub








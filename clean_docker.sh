#!/bin/bash
set -e
echo Apagando containers antigos
docker stop $(docker ps -a -q)
docker container prune
echo Apagando Images antigos
docker image prune -a
echo Apagando network
docker network rm myNetwork
echo Tudo limpo